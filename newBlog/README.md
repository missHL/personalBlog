# newblog

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
###########################################################
#全局安装vue-cli

npm install --global vue-cli

#用vue-cli来构建项目

vue init webpack mgSystem

#安装依赖

npm install

#打包上线

npm run build

#启动项目

npm run dev

http://localhost:8080/