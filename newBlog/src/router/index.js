import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/page/home/home'
import NewsView from '@/page/newsView/newsView'
import Login from '@/page/login/login'
import WriteEssay from '@/page/WriteEssay/WriteEssay'
import Register from '@/page/register/register'
import Management from '@/page/management/management'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/NewsView:id',
      name: 'NewsView',
      component: NewsView
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/WiteBlog',
      name: 'WiteBlog',
      component: WriteEssay
    },
    {
      path: '/Management',
      name: 'Management',
      component: Management
    }
  ]
})
