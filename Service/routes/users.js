const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require("fs");
const router = express.Router();
const userMode = require('../model/user');

router.get('/', (req, res) => {
  res.send('error!');
});

/* GET users listing. */
//添加用户
router.all('/addUser', (req, res, next) => {
  let content = req.body;
  let reqs = {
    enable: true, //用户是否有效
    nickName: content.nickName,
    userName: content.userName,
    password: content.password,
    email: content.email,
    introduction: '',
    portrait: ''
  }
  userMode.create(reqs, (err, result) => {
    if (err) {
      res.json({
        status: '1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'search successfully!',
      });
    }
  });
});

//用户登录
router.all('/login', (req, res) => {
  let content = req.body;
  userMode.find({
    userName: content.userName,
    password: content.password
  }, (err, result) => {
    if (err || result.length < 1) {
      console.log('ERROR::用户名或密码错误！');
      res.json({
        status: '1',
        msg: err ? err.message : 'ERROR::用户名或密码错误！'
      });
    } else {
      console.log('search successfully!');
      res.json({
        status: '0',
        msg: 'search successfully!',
        userName: result[0].userName
      });
    }
  });
});

//查询用户信息
router.all('/getUserInfo', (req, res) => {
  let content = req.body;
  userMode.find({
    userName: content.userName
  }, (err, result) => {
    if (!err && result.length > 0) {
      console.log(result);
      res.json({
        status: '0',
        msg: 'search successfully!',
        result: {
          userName: result[0].userName,
          email: result[0].email,
          enable: result[0].enable,
          introduction: result[0].introduction,
          nickName: result[0].nickName,
          portrait: result[0].portrait
        }
      });
    } else {
      res.json({
        status: '1',
        msg: err ? err.message : '查询用户详情失败！'
      });
    }
  });
});

//修改用户信息
router.all('/setUserInfo', (req, res) => {
  let content = req.body;
  userMode.update({'userName': content.userName}, {$set:{'introduction': content.introduction}}, (err, doc) => {
    let resp = {
      status: '0',
      msg: '修改成功'
    };
    if (err){
      resp.status = '1';
      resp.msg = err;
    } 
    res.json(resp);
  });
});

//创建文件夹
var createFileDirectory = function(path) {
  try {
      //检测文件夹是否存在，不存在抛出错误
      fs.accessSync(path);
  } catch (error) {
      //创建文件夹
      fs.mkdirSync(path);
  }
}

//multer文件的硬盘存储模式
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    //先创建路径在保存
    let uploadPath = path.join(__dirname , '../public/images')
    createFileDirectory(uploadPath);
    //指定文件保存路径
    cb(null, path.join(__dirname, '../public/images'));
  },
  filename: function (req, file, cb) {
    // 将保存文件名设置为 时间戳 + 文件原始名，比如 151342376785-123.jpg
    cb(null, Date.now() + '-' + file.originalname);
  }
});

let upload = multer({
  storage: storage
});

//updateFile更新用户头像测试
router.all('/updateFile', upload.single('file'), function (req, res) {
  let avatar = req.file
  let portrait = '/public/images/' + avatar.filename;
  userMode.update({'userName': req.body.userName}, {$set:{'portrait': portrait}}, (err, doc) => {
    let resp = {
      status: '0',
      msg: '图片上传成功！'
    };
    if (err){
      resp.status = '1';
      resp.msg = err;
    } 
    res.json(resp);
  });
});

module.exports = router;