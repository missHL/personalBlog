const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require("fs");
const router = express.Router();

router.get('/', (req, res) => {
  res.send('error!');
});

//创建文件夹
var createFileDirectory = function(path) {
  try {
      //检测文件夹是否存在，不存在抛出错误
      fs.accessSync(path);
  } catch (error) {
      //创建文件夹
      fs.mkdirSync(path);
  }
}

//multer文件的硬盘存储模式
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    //先创建路径在保存
    let uploadPath = path.join(__dirname , '../public/images')
    createFileDirectory(uploadPath);
    //指定文件保存路径
    cb(null, path.join(__dirname, '../public/images'));
  },
  filename: function (req, file, cb) {
    console.log(file)
    // 将保存文件名设置为 时间戳 + 文件原始名，比如 151342376785-123.jpg
    cb(null, Date.now() + '-' + file.originalname);
  }
});

let upload = multer({
  storage: storage
});

//updateFile更新用户头像测试
router.all('/uploadPicture', upload.single('file'), function (req, res) {
  let avatar = req.file
  console.log(req.body.userName);
  console.log(avatar)
  console.log(req.body)

  res.status(200).send(req.body.filename);
});

module.exports = router;