const express = require('express');
const router = express.Router();
const blogs = require('../model/blog');
const leaveMsg = require('../model/leaveMsg');

/* GET users listing. */
router.get('/', (req, res) => {
  res.send('开始查询啥嘞！');
});

//查询所有文章列表
router.all('/getBlogList', (req, res) => {
  blogs.find({}, (err, result) => {
    if (err) {
      res.json({
        status:'1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'search list successful!',
        result:{
          count: result.length,
          list: result
        }
      })
    }
  });
});

//查询单个文章详情信息
router.all('/getContentDetail', (req, res) => {
  let content = req.body;
  blogs.find({_id: content.id}, (err, result) => {
    if (err) {
      res.json({
        status:'1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'search list successful!',
        result:{
          count: result.length,
          content: result
        }
      })
    }
  });
});

//查询文章留言信息
router.all('/getMessageList', (req, res) => {
  let content = req.body;
  leaveMsg.find({contentId: content.id}, (err, result) => {
    if (err) {
      res.json({
        status:'1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'search Message successful!',
        result:{
          count: result.length,
          list: result
        }
      })
    }
  });
});

//写入游客文章留言
router.all('/setMessageList', (req, res) => {
  let content = req.body;
  let msgContent = {
    contentId: content.contentId,
    leaveMsg: content.leaveMsg,
    name: content.name,
    email: content.email,
    netUrl: content.netUrl,
    date: content.date
  }
  leaveMsg.create(msgContent, (err, result) => {
    if (err) {
      res.json({
        status:'1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'Successfully recorded tourist message!',
      });
    }
  })
});

//发布文章
router.all('/postBlog', (req, res) => {
  let content = req.body;
  let blogContent = {
    title: content.title,//博客标题
    source: content.source,//内容来源
    contentText: content.content,//正文
    label: content.label,//标签
    releaseTime: new Date,//发布时间
    clickRate: 0//点击量
  }
  blogs.create(blogContent, (err, result) => {
    if (err) {
      res.json({
        status:'1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: 'The article was published successfully!',
      });
    }
  })
})

//条件查询，根据标签条件查询文章列表 Conditional query
router.all('/conditionQuery', (req, res) => {
  
});

module.exports = router;
