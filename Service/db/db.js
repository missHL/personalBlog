const mongoose = require('mongoose');
const dbUrl = 'mongodb://localhost:27017/blog';
mongoose.connect(dbUrl);

//数据库连接成功
mongoose.connection.on('connected', function(){
    console.log('Monngoose 连接成功：' + dbUrl)
})

//数据库连接异常
mongoose.connection.on('error', function(err){
    console.log('连接失败：' + err)
})

//断开连接
mongoose.connection.on('disconnected', function(){
    console.log('断开连接！')
})

module.exports = mongoose;