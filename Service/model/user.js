const db = require('../db/db');
const Schema = db.Schema;

let UserSchema = new Schema({
    enable: { type: Boolean, default: true }, //用户是否有效
    nickName: String,
    userName: String,
    password: String,
    email: String,
    introduction: String,
    portrait: String
},{
    collection: 'user'
});
 
module.exports = db.model("user", UserSchema);