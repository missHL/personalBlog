const db = require('../db/db');
const Schema = db.Schema;

let LeaveMsg = new Schema({
    contentId: {type: String},//文件ID
    leaveMsg: {type: String},//留言信息
    name: {type: String},//留言者名字
    email: {type: String},//留言者邮箱
    netUrl: {type: String},//留言者网站
    date: {type: String}
},{
    collection: 'leavemsg'
});
 
module.exports = db.model("leavemsg", LeaveMsg);