const db = require('../db/db');
const Schema = db.Schema;

let BlogSchema = new Schema({
    title: {type: String},//博客标题
    source: {type: String},//内容来源
    contentText: {type: String},//正文
    label: {type: Array},//标签
    releaseTime: {type: Date},//发布时间
    clickRate: {type: Number}//点击量
},{
    collection: 'blogSet'
});
 
module.exports = db.model("blogSet", BlogSchema);